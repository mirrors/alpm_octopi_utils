## This is derived work from alpm_utils code, written by Guillaume Benoit.

You'll need Vala compiler in order to build this library!

### Steps for compilation:

```
$ git clone https://github.com/aarnt/alpm_octopi_utils
$ cd alpm_octopi_utils
$ mkdir build_dir && cd build_dir
$ cmake -DCMAKE_INSTALL_PREFIX=/usr ..
$ make
# make install
```
