# Distributed under the OSI-approved BSD 3-Clause License.
# See https://cmake.org/licensing for details.

#[=======================================================================[.rst:
alpm
----

Finds the alpm library.

Imported Targets
^^^^^^^^^^^^^^^^

This module was generated by "generate_find_module" and provides the following IMPORTED targets, if found:

``alpm::alpm``
  The alpm library

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

``alpm_FOUND``
  True if the system has the alpm library.
``alpm_VERSION``
  The version of the alpm library which was found.
``alpm_INCLUDE_DIRS``
  Include directories needed to use alpm.
``alpm_LIBRARIES``
  Libraries needed to link to alpm.

Cache Variables
^^^^^^^^^^^^^^^

The following cache variables may also be set:

``alpm_INCLUDE_DIR``
  The directory containing ``alpm.h``.
``alpm_LIBRARY``
  The path to the alpm library.

#]=======================================================================]

set(VERBOSE_FIND_MODULE OFF CACHE BOOL "Enable to print debug infos in the generated Find[...].cmake files")

find_package(PkgConfig)
pkg_check_modules(PC_alpm QUIET alpm)
find_path(alpm_INCLUDE_DIR NAMES alpm.h
    HINTS  
    PATHS  ${PC_alpm_INCLUDE_DIRS}
    PATH_SUFFIXES 
    
    
    
    
    
    
      
)

find_library(alpm_LIBRARY NAMES alpm
    HINTS 
    PATHS  ${PC_alpm_LIBRARY_DIRS}
    PATH_SUFFIXES 
    
    
    
    
    
    
      
)

set(Foo_VERSION ${PC_alpm_VERSION})
set(Foo_VERSION_STRING ${Foo_VERSION})

if(${VERBOSE_FIND_MODULE})
    message(STATUS "Running Findalpm.cmake")
    if(${PC_alpm_FOUND})
        message(STATUS "   Found PkgConfig module with name 'alpm':")
        message(STATUS "       alpm_LIBRARIES: ${PC_alpm_LIBRARIES}")
        message(STATUS "       alpm_LIBRARY_DIRS: ${PC_alpm_LIBRARY_DIRS}")
        message(STATUS "       alpm_LDFLAGS: ${PC_alpm_LDFLAGS}")
        message(STATUS "       alpm_LDFLAGS_OTHER: ${PC_alpm_LDFLAGS_OTHER}")
        message(STATUS "       alpm_INCLUDE_DIRS: ${PC_alpm_INCLUDE_DIRS}")
        message(STATUS "       alpm_CFLAGS: ${PC_alpm_CFLAGS}")
        message(STATUS "       alpm_CFLAGS_OTHER: ${PC_alpm_CFLAGS_OTHER}")
    else()
        message(STATUS "   No PkgConfig module found with name 'alpm'")
    endif()
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(alpm FOUND_VAR alpm_FOUND REQUIRED_VARS alpm_LIBRARY alpm_INCLUDE_DIR VERSION_VAR alpm_VERSION)

if(alpm_FOUND)
    set(alpm_LIBRARIES ${alpm_LIBRARY})
    if(PC_alpm_INCLUDE_DIRS)
        set(alpm_INCLUDE_DIRS ${PC_alpm_INCLUDE_DIRS})
    else()
        set(alpm_INCLUDE_DIRS ${alpm_INCLUDE_DIR})
    endif()
    set(alpm_DEFINITIONS ${PC_alpm_CFLAGS_OTHER})
endif()

if(alpm_FOUND AND NOT TARGET alpm::alpm)
    add_library(alpm::alpm UNKNOWN IMPORTED)
    set_target_properties(alpm::alpm PROPERTIES IMPORTED_LOCATION "${alpm_LIBRARY}" INTERFACE_COMPILE_OPTIONS "${PC_alpm_CFLAGS_OTHER}" INTERFACE_INCLUDE_DIRECTORIES "${alpm_INCLUDE_DIRS}")
    if(${VERBOSE_FIND_MODULE})
        message(STATUS "   Creating IMPORTED target 'alpm::alpm':")
        message(STATUS "       IMPORTED_LOCATION: ${alpm_LIBRARY}")
        message(STATUS "       INTERFACE_COMPILE_OPTIONS: ${PC_alpm_CFLAGS_OTHER}")
        message(STATUS "       INTERFACE_INCLUDE_DIRECTORIES: ${alpm_INCLUDE_DIRS}")
    endif()
endif()

mark_as_advanced(
  alpm_INCLUDE_DIR
  alpm_LIBRARY
)
